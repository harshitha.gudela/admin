from .models import admin1
from django import forms
from .admin import Details

class Form(forms.ModelForm):
    class Meta:
        model = admin1
        fields = "__all__"