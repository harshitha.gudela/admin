from django.shortcuts import render, redirect
# Create your views here.
from .models import admin1
from .forms import Form
from django.contrib import messages
from .admin import Details

def Insert_view(request):
    form = Form()
    if request.method == "POST":
        form = Form(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Data Inserted Successfully")
            return redirect("/")
    # print(form)
    return render(request, "insert.html", {"form": form})


def show(request):
    ad = admin1.objects.all()
    return render(request, 'show.html', {"ad": ad})


def delete(request, id):
    ad= admin1.objects.get(id=id)
    ad.delete()
    messages.success(request,"Data Deleted Successfully")
    return redirect("/show")


def update(request, id):
    ad = admin1.objects.get(id=id)
    form = Form(request.POST,instance=ad)
    if form.is_valid():
        form.save(commit=True)
        messages.success(request, "Data Updated Successfully")
        return redirect("/show")

    return render(request,'update.html',{'ad':ad})